import os

from fabric.api import *
from fabric.contrib.files import upload_template, append, exists

from util import validate_hostname, validate_slug, validate_stage
import nginx

__author__ = "Sina Khelil"

env.user = 'root'
env.use_ssh_config = True
env.hosts = ['host2']

deploy_files = {
    'nginx': {
        'template': 'nginx_host_conf',
        'dest_filename': '{site_name}-{site_stage}',
        'dest_directory': '/etc/nginx/sites-available/',
        'post_command': 'ln -sf /etc/nginx/sites-available/{site_name}-{site_stage} '
                        '/etc/nginx/sites-enabled/{site_name}-{site_stage}'
    },
    'supervisor': {
        'template': 'supervisor_conf',
        'dest_filename': '{site_name}-{site_stage}.conf',
        'dest_directory': '/etc/supervisor/conf.d',
    },
    'uwsgi': {
        'template': 'uwsgi_params_conf',
        'dest_filename': 'uwsgi_params',
        'dest_directory': '/var/www/{site_name}/{site_stage}',
    }
}


def create_host_file(template, template_dir, file_context, destination):
    dest_host = destination.format(**file_context)
    upload_template(template, dest_host, context=file_context, use_jinja=True, template_dir=template_dir,
                    keep_trailing_newline=True)


def install_letsencrypt():
    with cd('/usr/local/bin'):
        run('wget https://dl/eff/org/certbot-auto')
        run('chmod +x certbot-auto')


def generate_letsencrypt_cert(domains, env):
    dest_file = '/etc/nginx/sites-enabled/certbot-{site_name}-{site_stage}'.format(**env)
    upload_template('letsencrypt_nginx_conf', dest_file, context=env,
                    use_jinja=True, template_dir='templates/letsencrypt', keep_trailing_newline=True)
    run('service nginx reload')
    run('/usr/local/bin/certbot-auto certonly --webroot -w /usr/share/nginx/html {0}'.format(
        ' '.join(['-d {0}'.format(domain) for domain in domains])))
    run('rm -rf {0}'.format(dest_file))
    run('service nginx reload')


def install_python_tools():
    run('wget https://bootstrap.pypa.io/get-pip.py; python get-pip.py; rm get-pip.py;')
    run('pip install virtualenvwrapper')
    env_vars = '''
    export WORKON=/.virtualenvs
    source /usr/local/bin/virtualenvwrapper.sh
    '''
    append('/etc/environment', env_vars)


def set_unattended_upgrades_settings():
    upload_template('periodic_conf', '/etc/apt/apt.conf.d/10periodic',template_dir='templates/unattended-upgrades')


@task
def install_base_packages():
    packages = ['build-essential', 'gcc', 'git', 'zsh', 'nginx', 'ufw', 'fail2ban', 'logwatch', 'unattended-upgrades',
                'supervisor']
    run('aptitude update')
    run('DEBIAN_FRONTEND=noninteractive aptitude -y -q upgrade')
    run('DEBIAN_FRONTEND=noninteractive aptitude -y -q install {0}'.format(' '.join(packages)))


@task
def setup_firewall():
    ufw_commands = ['ufw default deny incoming', 'ufw default allow outgoing', 'ufw limit ssh', 'ufw allow ssh',
                    'ufw allow http', 'ufw allow https', 'ufw enable']
    for ufw_command in ufw_commands:
        run(ufw_command)


@task
def setup_new_host():
    prompt('Enter Site Domain:', key='site_domain', default='www.example.com', validate=validate_hostname)
    prompt('Enter Site Name (no spaces):', key='site_name', default='site-name', validate=validate_slug)
    prompt('Enter stage (stage or live):', key='site_stage', default='stage', validate=validate_stage)
    prompt('Secure site (y or n):', key='secure_site', default='n')
    if env['secure_site']:
        prompt('Force secure (y or n):', key='force_secure_site', default='n')
    prompt('Redirect domains to Site Domain (space delimited domains):', key='redir_site_domains', default='')
    if env['secure_site']:
        domains = [env['site_domain']]
        if env['redir_site_domains'].strip():
            domains = domains + env['redir_site_domains'].split(',')
        generate_letsencrypt_cert(domains, env)
    for key, val in deploy_files.items():
        dest_directory = val['dest_directory'].format(**env)
        dest_filename = val['dest_filename'].format(**env)

        if not exists(dest_directory):
            run('mkdir -p {0}'.format(dest_directory))
            sudo('chmod 775 {0}'.format(dest_directory))

        destination = os.path.join(dest_directory, dest_filename)
        create_host_file(val['template'], 'templates/{0}'.format(key), env, destination)
        if 'post_command' in val:
            run(val['post_command'].format(**env))
