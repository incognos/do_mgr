from fabric.api import *

__author__ = 'Sina Khelil'


@task
def reload_nginx():
    run('service nginx reload')
