import re

__author__ = 'Sina Khelil'


def is_valid_hostname(hostname):
    if len(hostname) > 255:
        return False
    if hostname[-1] == ".":
        hostname = hostname[:-1] # strip exactly one dot from the right, if present
    allowed = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)
    return all(allowed.match(x) for x in hostname.split("."))


def validate_hostname(hostname):
    if is_valid_hostname(hostname):
        return hostname
    else:
        raise Exception('Bad hostname')


def validate_stage(stage):
    if stage in ['stage', 'live']:
        return stage
    else:
        raise Exception('Select "stage" or "live"')


def validate_slug(value):
    if re.match('^[-a-zA-Z0-9_]+$', value):
        return value
    else:
        raise Exception('Must be a slug value')


def validate_boolean(value):
    return value.lower() in ['y', 'yes', 't', 'true', '1']
