from fabric.api import *
from fabric.contrib.files import upload_template

__author__ = 'Sina Khelil'


@task
def update():
    run('supervisorctl update')
