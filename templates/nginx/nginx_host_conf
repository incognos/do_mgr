upstream {{ site_name }}-{{ site_stage }} {
    server unix:///tmp/{{ site_name }}-{{ site_stage }}.sock;
}

{% if force_secure_site %}
server {
   listen 80;
   server_name {{ redir_site_domains }};
   return 301 https://{{ site_domain }}$request_uri;
}
{% elif redir_site_domains %}
server {
    listen 80;
    server_name {{ redir_site_domains }};
    return 301 http://{{ site_domain }}$request_uri;
}
{% endif  %}

server {
    {% if secure_site %}listen 443 ssl;{% endif %}
    {% if not force_secure_site %}listen 80;{% endif %}
    server_name {{ site_domain }};

    client_max_body_size 20M;

    {% if secure_site %}
    ssl on;
    ssl_certificate /etc/letsencrypt/live/{{ site_domain }}/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/{{ site_domain }}/privkey.pem;
    add_header X-Frame-Options DENY;
    add_header X-Content-Type-Options nosniff;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_dhparam /etc/ssl/certs/dhparam.pem;
    ssl_ciphers 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA';
    ssl_session_timeout 1d;
    ssl_session_cache shared:SSL:10m;
    ssl_stapling on;
    ssl_stapling_verify on;
    add_header Strict-Transport-Security max-age=15768000;
    {% endif %}

    access_log /var/log/nginx/{{ site_name }}-{{ site_stage }}-access.log;
    error_log /var/log/nginx/{{ site_name }}-{{ site_stage }}-error.log;

    gzip            on;
    gzip_min_length 1000;
    gzip_proxied    expired no-cache no-store private auth;
    gzip_buffers  4 32k;
    gzip_types    text/plain application/x-javascript text/xml text/css;
    gzip_vary on;

    location /robots.txt {
        alias /var/www/{{ site_name }}/{{ site_stage }}/robots.txt;
    }

    location /static/ {
        alias /var/www/{{ site_name }}/{{ site_stage }}/static/;
    }

    location /media/ {
        alias /var/www/{{ site_name }}/{{ site_stage }}/media/;
    }

    sendfile on;

    location / {
        uwsgi_pass {{ site_name }}-{{ site_stage }};
        include /etc/nginx/uwsgi_params;
    }
}